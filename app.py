from flask import Flask
from celery import Celery
from project import create_app, ext_celery


# app = Flask(__name__)
app = create_app()
celery = ext_celery.celery


@app.route('/')
def home():
    return "hello"
